extends Node
var choice: int


func _ready():
	OS.window_fullscreen = false
	Engine.time_scale = 1
	Score_count.score = 0
	get_node("Disappear").interpolate_property(get_node("2Player"),"modulate",Color.white,Color(1,1,1,0),.75,Tween.TRANS_LINEAR,Tween.EASE_IN)
	get_node("Appear").interpolate_property(get_node("2Player"),"modulate",Color(1,1,1,0),Color.white,.75,Tween.TRANS_LINEAR,Tween.EASE_IN)
	Input.connect("joy_connection_changed",self,"changed")
	get_node("Player1").modulate = Color.green
	get_node("Player1").text = "P1 connected"
	if Input.get_connected_joypads().size() == 2:
		get_node("2Player").modulate = Color.white
		get_node("Player1").modulate = Color.green
		get_node("Player2").modulate = Color.green
		get_node("Player2").text = "P2 connected"

func _input(_event):
	if Input.is_action_pressed("fps_counter"):
		if fps_counter.fps_counter == false:
			fps_counter.fps_counter = true
			get_node("FPS-setting").text = "FPS On"
		else:
			fps_counter.fps_counter = false
			get_node("FPS-setting").text = "FPS Off"
	if Input.is_action_just_pressed("x") || Input.is_action_just_pressed("ui_accept"):
		choice = 1
		get_node("Fadein").show()
		get_node("Fadein").fade_in()
	elif Input.is_action_just_pressed("x2"):
		choice = 2
		get_node("Fadein").show()
		get_node("Fadein").fade_in()

func changed(device,connected):
	if device == 1 && connected == true:
		get_node("Appear").interpolate_property(get_node("2Player"),"modulate",Color(1,1,1,0),Color.white,.25,Tween.TRANS_LINEAR,Tween.EASE_IN)
		$Appear.start()
		get_node("Player2").modulate = Color.green
		get_node("Player2").text = "P2 connected"
	elif device == 1 && connected == false:
		get_node("Disappear").interpolate_property(get_node("2Player"),"modulate",Color.white,Color(1,1,1,0),.25,Tween.TRANS_LINEAR,Tween.EASE_IN)
		$Disappear.start()
		get_node("Player2").modulate = Color(184,15,10,255)
		get_node("Player2").text = "P2 disconnected"
		
#func _process(delta):
#	print(Input.get_connected_joypads())

func _on_Fadein_fade_finished():
	if choice == 1:
		get_tree().change_scene("res://new_map_new.tscn")
		Mode.mode = false
	elif choice == 2:
		get_tree().change_scene("res://Main.tscn")
		Mode.mode = true
