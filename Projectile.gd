extends Area2D

var knock_direction
var speed = 1500
var mode
var knockback = false
var hit_type
var dir


# Called when the node enters the scene tree for the first time.
func _ready():
	rotation = dir.angle()

func _process(delta):
	$Type.play(mode)
	global_position += dir * speed * delta

func _on_Projectile_body_entered(body):
	if body.get_class() == "TileMap":
		queue_free()
	else:
		body.already_hit = false
		body.stun = 5
		body.projectile_mode = mode
		body.last_motion = dir
		body.hit_group = hit_type
		queue_free()


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
