extends Node2D
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const enemy_path  = ["res://Enemies!/Bio/banana.tscn","res://Enemies!/Bio/paper.tscn","res://Enemies!/Haz/spray.tscn","res://Enemies!/Haz/wash.tscn","res://Enemies!/Nonbio/plastic.tscn","res://Enemies!/Nonbio/water.tscn"]
const enemies = [preload("res://Enemies!/Bio/banana.tscn"),preload("res://Enemies!/Bio/paper.tscn"),preload("res://Enemies!/Haz/spray.tscn"),preload("res://Enemies!/Haz/wash.tscn"),preload("res://Enemies!/Nonbio/plastic.tscn"),preload("res://Enemies!/Nonbio/water.tscn")]

var entity
var timer = Timer.new()
# Called when the node enters the scene tree for the first time.

func _ready():
	Mode.boss = false
	Engine.time_scale = 1
	randomize()
	timer.wait_time = 6
	timer.connect("timeout", self ,"on_timeout_complete")
	timer.autostart = true
	add_child(timer)
	$Player.global_position = get_node("SpawnPoint").global_position
	timer.start()

func on_timeout_complete():
	if Mode.enemies < 3:
		entity = randi() % 6
		var enemy = enemies[entity].instance()
		match enemy_path[entity][15]:
			'B':
				enemy.type = "Biodegradable"
			'N':
				enemy.type = "Non-biodegradable"
			'H':
				enemy.type = "Hazardous"
		entity = randi() % 6
		add_child(enemy)
		Mode.enemies += 1
		enemy.global_position = get_node(str("EnemySpawnPoint",entity)).global_position

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):

