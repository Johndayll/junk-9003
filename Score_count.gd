extends Node
var score_timer = Timer.new()
var score = 0
signal killed

func _ready():
	score_timer.wait_time = 1
	score_timer.connect("timeout", self , "_score")
	score_timer.autostart = true
	add_child(score_timer)
func _score():
	score += 3
