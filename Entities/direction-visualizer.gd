extends Position2D


const SCALE_RANGE = Vector2(0.5, 1.0)
const SCALE_RATE = 4
var dir = Vector2()
var arrow_lastdirection
var controllerangle


func _ready():
	$Arrow.scale = Vector2(SCALE_RANGE.x, SCALE_RANGE.x)

func _physics_process(_elta):
	var LEFT = -int(Input.is_action_pressed("aim_left"))
	var RIGHT = int(Input.is_action_pressed("aim_right"))
	var UP = -int(Input.is_action_pressed("aim_up"))
	var DOWN = int(Input.is_action_pressed("aim_down"))
	
	dir.x = LEFT + RIGHT
	if -LEFT + RIGHT == 2:
		if Input.is_action_just_pressed("aim_left"):
			arrow_lastdirection = -1
		if Input.is_action_just_pressed("aim_right"):
			arrow_lastdirection = 1
		dir.x = arrow_lastdirection
	
	dir.y = UP + DOWN
	
	if -UP + DOWN == 2:
		if Input.is_action_just_pressed("aim_up"):
			arrow_lastdirection = -1
		if Input.is_action_just_pressed("aim_down"):
			arrow_lastdirection = 1
		dir.y = arrow_lastdirection
	var deadzone = .3
	var xAxisRL = Input.get_joy_axis(0,JOY_AXIS_2)
	var yAxisUD = Input.get_joy_axis(0,JOY_AXIS_3)
	if abs(xAxisRL) > deadzone || abs(yAxisUD) > deadzone:
		$Arrow.show()
		controllerangle = Vector2(xAxisRL, yAxisUD).angle()
		dir = Vector2(cos(controllerangle), sin(controllerangle))
		rotation = controllerangle
	elif dir != Vector2.ZERO:
		$Arrow.show()
		rotation = dir.angle()
	else:
		$Arrow.hide()
