extends Node2D

export(PackedScene) var Bullet

func _physics_process(delta):
	if not $CooldownTimer.is_stopped():
		return
	if Input.is_action_pressed("fire"):
		fire()

func fire():
	var direction = get_local_mouse_position().normalized()
	spawn_projectile(direction)
	$CooldownTimer.start()

func spawn_projectile(direction):
	var bullet = Bullet.instance()
	bullet.direction = direction
	bullet.set_as_toplevel(true)
	bullet.position = global_position
	add_child(bullet)

func _on_CooldownTimer_timeout():
	$CooldownTimer.stop()
